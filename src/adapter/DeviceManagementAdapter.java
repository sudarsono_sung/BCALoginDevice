package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class DeviceManagementAdapter {
	final static Logger logger = Logger.getLogger(DeviceManagementAdapter.class);

	public static boolean CheckLogin(model.mdlLoginDeviceManagement mdlLoginDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "loginDeviceManagement";
		mdlLog.SystemFunction = "CheckLogin";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlLoginDeviceManagement.SerialNumber;
		mdlLog.WSID = "";
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		boolean userExists = false;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "SELECT UserID, Password FROM ms_user WHERE UserID = ? AND Password = ? FETCH FIRST 1 ROWS ONLY";

			pstm = connection.prepareStatement(sql);
			pstm.setString(1, mdlLoginDeviceManagement.UserID);
			pstm.setString(2, mdlLoginDeviceManagement.Password);
			//execute query
			jrs = pstm.executeQuery();

			while(jrs.next()){
				userExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCALoginDevice, method : POST, function : CheckLogin, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception ex) {

			}
		}
		return userExists;
	}
}