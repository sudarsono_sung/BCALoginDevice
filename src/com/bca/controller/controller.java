package com.bca.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.DeviceManagementAdapter;
import adapter.LogAdapter;
import model.Globals;

@RestController
public class controller {
	final static Logger logger = Logger.getLogger(controller.class);

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

    @RequestMapping(value = "/login",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult LoginDeviceManagement(@RequestBody model.mdlLoginDeviceManagement param)
	{
		model.mdlAPIResult mdlLoginDeviceManagementResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();
		Gson gson = new Gson();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = "";
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "loginDeviceManagement";
		mdlLog.SystemFunction = "LoginDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		mdlResult.Result = "false";

		String UserID = (param.UserID == null) ? "" : param.UserID.trim();
		String Password = (param.Password == null) ? "" : param.Password.trim();
		Boolean checkLogin = false;

		if (UserID.equals("") || Password.equals("")){
			mdlErrorSchema.ErrorCode = "01";
			mdlMessage.Indonesian = "User ID atau Password invalid";
			mdlMessage.English = mdlLog.ErrorMessage = "Invalid User ID or Password";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
			mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
			LogAdapter.InsertLog(mdlLog);
			return mdlLoginDeviceManagementResult;
		}else{
			try {
				checkLogin = DeviceManagementAdapter.CheckLogin(param);
				if (checkLogin){
					mdlErrorSchema.ErrorCode = "00";
					mdlMessage.Indonesian = "Berhasil";
					mdlMessage.English = mdlLog.LogStatus = "Success";
					mdlErrorSchema.ErrorMessage = mdlMessage;
					mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
					mdlResult.Result = checkLogin.toString();
					mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
				}else{
					mdlErrorSchema.ErrorCode = "02";
					mdlMessage.Indonesian = "Password yang Anda masukkan salah";
					mdlMessage.English = mdlLog.ErrorMessage = "Wrong Password Input";
					mdlErrorSchema.ErrorMessage = mdlMessage;
					mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
					mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
				}

				String jsonIn = gson.toJson(param);
				logger.info("SUCCESS. API : BCALoginDevice, method : POST, jsonIn:" + jsonIn);
			  } catch (Exception ex){
				  mdlErrorSchema.ErrorCode = "03";
				  mdlMessage.Indonesian = "Gagal";
				  mdlMessage.English = mdlLog.ErrorMessage = "Failed";
				  mdlErrorSchema.ErrorMessage = mdlMessage;
				  mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				  mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
				  String jsonIn = gson.toJson(param);
				  logger.error("FAILED. API : BCALoginDevice, method : POST, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
			  }
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlLoginDeviceManagementResult;
	}

}
